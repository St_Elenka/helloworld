import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class HelloWorld {
    public static void main(String[] args) {
        DateTime dt = new DateTime();
        DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm:ss dd.MM.yyyy ZZ");
        System.out.println(fmt.print(dt));
    }
}